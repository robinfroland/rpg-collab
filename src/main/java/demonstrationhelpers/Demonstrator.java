package main.java.demonstrationhelpers;

/*
 Class description:
 ------------------
Demonstrates
*/

import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.*;
import main.java.characters.support.Priest;
import main.java.consolehelpers.Color;
import main.java.factories.CharacterFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;

import java.util.ArrayList;
import java.util.List;

public class Demonstrator {
    private List<Character> party = new ArrayList<>();
    private List<Weapon> weapons = new ArrayList<>();
    private List<Armor> armor = new ArrayList<>();
    private Character dummyCharacter = CharacterFactory.getCharacter(CharacterType.Warrior);
    private final int nParticipants = 8;

    public void outputDemonstration() {
        outputDemonstration(nParticipants);
    }

    public void outputDemonstration(int nParticipants) {
        party = Initializer.initParty(nParticipants);
        weapons = Initializer.initWeaponItems();
        armor = Initializer.initArmorItems();

        System.out.println("+-----------------------------------------+");
        System.out.println("|              DEMONSTRATION              |");
        System.out.println("+-----------------------------------------+");

        System.out.println("\n** PERFORMING ABILITIES WITH DEFAULT EQUIPMENT **\n");
        performAbilities();

        System.out.println("\n** CHANGING EQUIPMENT WITH RANDOM RARITY **\n");
        changeEquipment();

        System.out.println("\n** PERFORMING ABILITIES WITH NEW EQUIPMENT **\n");
        performAbilities();

        System.out.println("\n** TAKING PHYSICAL AND MAGICAL DAMAGE **\n");
        takeDamage();
    }

    private void performAbilities() {
        for (Character character: party) {
            if (character instanceof Attacker) {
                double attackVal = ((Attacker) character).attack();
                System.out.println(character + " dealt " + Color.RED + attackVal + Color.RESET + " in damage " + "\n");
            } else if (character instanceof Supporter) {
                // If supporter role, use dummyCharacter
                double supportVal = ((Supporter) character).support(dummyCharacter);
                String supportType = character instanceof Priest ? "shield " : "healing ";
                System.out.println(character + " supported " + dummyCharacter + " with " + Color.GREEN + supportVal + Color.RESET + " in " + supportType + "\n");
            }
        }
    }

    private void changeEquipment() {
        for (Character character : party) {
            Weapon prevWeapon = character.getWeapon();
            Armor prevArmor = character.getArmor();
            boolean weaponChanged = false;

            for (Weapon weapon : weapons) {
                boolean isNew = character.getWeapon().getClass() != weapon.getClass();
                // Try to equip new weapon if it is different from current
                if (isNew && !weaponChanged) {
                    // Tries to wield new weapon. Return success flag.
                    weaponChanged = character.equipWeapon(weapon);
                }
            }

            for (Armor armor : armor) {
                // Checks are unnecessary since character can only wield one armor. Only rarity will update.
                character.equipArmor(armor);
            }

            System.out.println("Changing equipment for " + character);
            System.out.println(prevWeapon + " with rarity " + Color.CYAN + prevWeapon.getRarity() + Color.RESET + " -> " + character.getWeapon() + " with rarity " + Color.CYAN + character.getWeapon().getRarity() + Color.RESET );
            System.out.println(prevArmor + " with rarity " + Color.CYAN + prevArmor.getRarityModifier() + Color.RESET + " -> " + character.getArmor() + " with rarity " + Color.CYAN + character.getArmor().getRarityModifier() + Color.RESET + "\n");
        }
    }

    private void takeDamage() {
        int damage = 50;
        for (Character character : party) {
            double prevHealth = character.getCurrentHealth();
            character.takeDamage(damage, DamageType.Magical);
            character.takeDamage(damage, DamageType.Physical);
            System.out.println(character + " takes " + Color.CYAN + damage + Color.RESET + " physical damage and " + Color.BLUE + damage + Color.RESET + " magical damage:");
            System.out.println("Updated health: " + Color.GREEN + prevHealth + Color.RESET + " -> " + Color.RED + character.getCurrentHealth() + Color.RESET + "\n");

        }
    }
    

}
