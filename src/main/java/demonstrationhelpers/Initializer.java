package main.java.demonstrationhelpers;

import main.java.basestats.ItemRarityModifiers;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.factories.ArmorFactory;
import main.java.factories.CharacterFactory;
import main.java.factories.SpellFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.SpellType;

import java.util.ArrayList;
import java.util.List;

public class Initializer {

    public static List<Character> initParty(int nParticipants) {
        List<Character> party = new ArrayList<>();
        for (int i = 0; i < nParticipants; i++) {
            switch (i) {
                case 0 -> party.add(CharacterFactory.getCharacter(CharacterType.Druid, SpellFactory.getSpell(SpellType.Regrowth)));
                case 1 -> party.add(CharacterFactory.getCharacter(CharacterType.Mage, SpellFactory.getSpell(SpellType.ArcaneMissile)));
                case 2 -> party.add(CharacterFactory.getCharacter(CharacterType.Paladin));
                case 3 -> party.add(CharacterFactory.getCharacter(CharacterType.Priest, SpellFactory.getSpell(SpellType.Rapture)));
                case 4 -> party.add(CharacterFactory.getCharacter(CharacterType.Ranger));
                case 5 -> party.add(CharacterFactory.getCharacter(CharacterType.Rogue));
                case 6 -> party.add(CharacterFactory.getCharacter(CharacterType.Warlock, SpellFactory.getSpell(SpellType.ChaosBolt)));
                case 7 -> party.add(CharacterFactory.getCharacter(CharacterType.Warrior));
            }
        }
        return party;
    }

    public static List<Weapon> initWeaponItems() {
        List<Weapon> weapons = new ArrayList<>();
        System.out.println("INSTANTIATING WEAPONS..");
        // Init weapons
        weapons.add(WeaponFactory.getWeapon(WeaponType.Axe, getRandomRarity()));
        weapons.add(WeaponFactory.getWeapon(WeaponType.Dagger, getRandomRarity()));
        weapons.add(WeaponFactory.getWeapon(WeaponType.Sword, getRandomRarity()));
        weapons.add(WeaponFactory.getWeapon(WeaponType.Hammer, getRandomRarity()));
        weapons.add(WeaponFactory.getWeapon(WeaponType.Mace, getRandomRarity()));
        weapons.add(WeaponFactory.getWeapon(WeaponType.Staff, getRandomRarity()));
        weapons.add(WeaponFactory.getWeapon(WeaponType.Wand, getRandomRarity()));
        weapons.add(WeaponFactory.getWeapon(WeaponType.Bow, getRandomRarity()));
        weapons.add(WeaponFactory.getWeapon(WeaponType.Crossbow, getRandomRarity()));
        weapons.add(WeaponFactory.getWeapon(WeaponType.Gun, getRandomRarity()));

        return weapons;
    }

    public static List<Armor> initArmorItems() {
        List<Armor> armor = new ArrayList<>();
        System.out.println("INSTANTIATING ARMOR...");
        // Init armor
        armor.add(ArmorFactory.getArmor(ArmorType.Cloth, getRandomRarity()));
        armor.add(ArmorFactory.getArmor(ArmorType.Leather, getRandomRarity()));
        armor.add(ArmorFactory.getArmor(ArmorType.Mail, getRandomRarity()));
        armor.add(ArmorFactory.getArmor(ArmorType.Plate, getRandomRarity()));

        return armor;
    }

    private static double getRandomRarity() {
        int randomNum = (int)(Math.random() * (4 - 1 + 1) + 1);
        return switch (randomNum) {
            case 1 -> ItemRarityModifiers.UNCOMMON_RARITY_MODIFIER;
            case 2 -> ItemRarityModifiers.RARE_RARITY_MODIFIER;
            case 3 -> ItemRarityModifiers.EPIC_RARITY_MODIFIER;
            case 4 -> ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER;
            default -> ItemRarityModifiers.COMMON_RARITY_MODIFIER;
        };
    }
}
