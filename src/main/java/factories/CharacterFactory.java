package main.java.factories;
// Imports
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.caster.Mage;
import main.java.characters.caster.Warlock;
import main.java.characters.melee.Paladin;
import main.java.characters.melee.Rogue;
import main.java.characters.melee.Warrior;
import main.java.characters.ranged.Ranger;
import main.java.characters.support.Druid;
import main.java.characters.support.Priest;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.ShieldingSpell;
import main.java.spells.abstractions.Spell;

/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Character as a return type when refactored to be good OO design.
*/
// TODO Once characters can be made with the right compositions, then implement the factory
public class CharacterFactory {

    public static Character getCharacter(CharacterType characterType) {
        return switch (characterType) {
            case Paladin -> new Paladin();
            case Ranger -> new Ranger();
            case Rogue -> new Rogue();
            case Warrior -> new Warrior();
            case Mage -> new Mage(null);
            case Warlock -> new Warlock(null);
            case Druid -> new Druid(null);
            case Priest -> new Priest(null);
        };
    }

    public static Character getCharacter(CharacterType characterType, Spell spell) {
        return switch (characterType) {
            case Mage -> new Mage((DamagingSpell) spell);
            case Warlock -> new Warlock((DamagingSpell) spell);
            case Druid -> new Druid((HealingSpell) spell);
            case Priest -> new Priest((ShieldingSpell) spell);
            default -> null;
        };
    }
}