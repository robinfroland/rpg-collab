package main.java.factories;
// Imports
import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.armor.Mail;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;

/*
 This factory exists to be responsible for creating new Armor.
 Object is replaced with some Item abstraction.
*/
public class ArmorFactory {

    public static Armor getArmor(ArmorType armorType){
        return switch (armorType) {
            case Cloth -> new Cloth();
            case Leather -> new Leather();
            case Mail -> new Mail();
            case Plate -> new Plate();
        };
    }

    public static Armor getArmor(ArmorType armorType, double itemRarityModifier){
        return switch (armorType) {
            case Cloth -> new Cloth(itemRarityModifier);
            case Leather -> new Leather(itemRarityModifier);
            case Mail -> new Mail(itemRarityModifier);
            case Plate -> new Plate(itemRarityModifier);
        };
    }
}
