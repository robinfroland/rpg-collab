package main.java;

import main.java.demonstrationhelpers.Demonstrator;

public class Main {
    public static void main(String[] args) {
        Demonstrator demo = new Demonstrator();
        demo.outputDemonstration();
    }
}
