package main.java.characters.ranged;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.characters.Ranged;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponType;

/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger extends Ranged {

    public Ranger() {
        super(
                CharacterBaseStatsDefensive.RANGER_BASE_HEALTH,
                CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER,
                WeaponFactory.getWeapon(WeaponType.Bow), ArmorFactory.getArmor(ArmorType.Mail, 1)
        );
    }
}
