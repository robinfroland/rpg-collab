package main.java.characters.support;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.characters.Shielder;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.ShieldingSpell;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest extends Shielder {

    public Priest(ShieldingSpell spell) {
        super(
                CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH,
                CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES,
                WeaponFactory.getWeapon(WeaponType.Staff),
                ArmorFactory.getArmor(ArmorType.Leather, 1),
                spell
        );
    }
}
