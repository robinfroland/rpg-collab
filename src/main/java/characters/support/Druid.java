package main.java.characters.support;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.characters.Healer;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.HealingSpell;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid extends Healer {

    public Druid(HealingSpell spell) {
        super(
                CharacterBaseStatsDefensive.DRUID_BASE_HEALTH,
                CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES,
                WeaponFactory.getWeapon(WeaponType.Staff),
                ArmorFactory.getArmor(ArmorType.Leather, 1),
                spell
        );
    }
}
