package main.java.characters.abstractions;

import main.java.consolehelpers.Color;
import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;

public abstract class Character {
    // Equipment
    private Armor armor;
    private Weapon weapon;

    // Base stats defensive
    private double basePhysReductionPercent;
    private double baseMagicReductionPercent;

    // Active trackers and flags
    private double currentHealth;
    private double currentHealthMax; // potential health with gear etc.
    private double shieldHealth; // barrier-health.

    private Boolean isDead;

    public Character(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, Weapon weapon, Armor armor) {
        this.currentHealth = baseHealth;
        this.currentHealthMax = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        this.basePhysReductionPercent = basePhysReductionPercent;
        this.baseMagicReductionPercent = baseMagicReductionPercent;
        this.weapon = weapon;
        this.armor = armor;
        this.isDead = false;
    }

    // Public getters for stats and state
    public double getCurrentHealth() {
        return Math.floor(currentHealth * 100) / 100;
    }
    public double getCurrentMaxHealth() {
        return currentHealthMax;
    }
    public Boolean getDead() {
        return isDead;
    }
    public Weapon getWeapon() { return weapon; }
    public Armor getArmor() { return armor; }


    // Character behaviours
    public double takeDamage(double damage, DamageType damageType) {
        double damageTaken = switch (damageType) {
            case Physical -> damage * (1 - ((basePhysReductionPercent / 100) * armor.getPhysRedModifier() * armor.getRarityModifier()));
            case Magical -> damage * (1 - ((baseMagicReductionPercent / 100) * armor.getMagicRedModifier() * armor.getRarityModifier()));
        };

        if ((currentHealth -= damageTaken) <= 0) isDead = true;
        return damageTaken;
    }

    public void receiveHealing(double healing){
        currentHealth += healing;
        if (currentHealth > currentHealthMax){
            currentHealth = currentHealthMax;
        }
    }

    public void receiveShield(double shield){
        if (shieldHealth > 0){
            shieldHealth = 0;
        }
        shieldHealth = shield;
    }



    // Equipment behaviours
    public boolean equipWeapon(Weapon weapon) {
        if (this.weapon.getClass().getSuperclass() == weapon.getClass().getSuperclass()) {
            this.weapon = weapon;
            return true;
        }
        return false;
    }

    public boolean equipArmor(Armor armor) {
        if (this.armor.getClass() == armor.getClass()) {
            this.armor = armor;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return Color.YELLOW + this.getClass().getSimpleName() + Color.RESET;
    }
}



