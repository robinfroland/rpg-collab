package main.java.characters.abstractions.characters;

import main.java.characters.abstractions.Attacker;
import main.java.characters.abstractions.Character;
import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.Weapon;

public abstract class Ranged extends Character implements Attacker {
    private double baseAttackPower;

    public Ranged(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, double baseAttackPower, Weapon weapon, Armor armor) {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, weapon, armor);
        this.baseAttackPower = baseAttackPower;
    }

    @Override
    public double attack() {
        double attackVal = baseAttackPower * ((RangedWeapon) getWeapon()).getRangedPowerModifier() * getWeapon().getRarity();
        return Math.floor(attackVal * 100) / 100;
    }
}
