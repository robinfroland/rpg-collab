package main.java.characters.abstractions.characters;

import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.Supporter;
import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.spells.abstractions.HealingSpell;

public abstract class Healer extends Character implements Supporter {
    private HealingSpell spell;

    public Healer(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, Weapon weapon, Armor armor, HealingSpell spell) {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, weapon, armor);
        this.spell = spell;
    }

    @Override
    public double support(Character character) {
        double healingValue = spell.getHealingAmount()
                * ((MagicWeapon) getWeapon()).getMagicPowerModifier()
                * getWeapon().getRarity();

        character.receiveHealing(healingValue);
        return Math.floor(healingValue * 100) / 100;
    }
}
