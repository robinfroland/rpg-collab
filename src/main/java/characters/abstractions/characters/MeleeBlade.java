package main.java.characters.abstractions.characters;

import main.java.characters.abstractions.Attacker;
import main.java.characters.abstractions.Character;
import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.BladeWeapon;
import main.java.items.weapons.abstractions.Weapon;

public abstract class MeleeBlade extends Character implements Attacker {
    private double baseAttackPower;

    public MeleeBlade(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, double baseAttackPower, Weapon weapon, Armor armor) {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, weapon, armor);
        this.baseAttackPower = baseAttackPower;
    }

    @Override
    public double attack() {
        double attackVal = baseAttackPower * ((BladeWeapon) getWeapon()).getBladedPowerModifier() * getWeapon().getRarity();
        return Math.floor(attackVal * 100) / 100;
    }
}
