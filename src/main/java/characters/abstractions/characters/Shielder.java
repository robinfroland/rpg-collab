package main.java.characters.abstractions.characters;

import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.Supporter;
import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.spells.abstractions.ShieldingSpell;

public abstract class Shielder extends Character implements Supporter {
    private ShieldingSpell spell;

    public Shielder(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, Weapon weapon, Armor armor, ShieldingSpell spell) {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, weapon, armor);
        this.spell = spell;
    }

    @Override
    public double support(Character character) {
        double shieldValue = (character.getCurrentMaxHealth()
                * spell.getAbsorbShieldPercentage())
                + ((MagicWeapon) getWeapon()).getMagicPowerModifier()
                * getWeapon().getRarity();
        character.receiveShield(shieldValue);
        return Math.floor(shieldValue * 100) / 100;
    }
}
