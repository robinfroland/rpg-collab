package main.java.characters.abstractions.characters;

import main.java.characters.abstractions.Attacker;
import main.java.characters.abstractions.Character;
import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.spells.abstractions.DamagingSpell;


public abstract class Caster extends Character implements Attacker {
    private double baseMagicPower;
    private DamagingSpell spell;


    public Caster(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, double baseMagicPower, Weapon weapon, Armor armor, DamagingSpell spell) {
        super(baseHealth, basePhysReductionPercent, baseMagicReductionPercent, weapon, armor);
        this.baseMagicPower = baseMagicPower;
        this.spell = spell;
    }

    @Override
    public double attack() {
        double attackVal = baseMagicPower * ((MagicWeapon) getWeapon()).getMagicPowerModifier() * spell.getSpellDamageModifier() * getWeapon().getRarity();
        return Math.floor(attackVal * 100) / 100;
    }
}
