package main.java.characters.abstractions;

public enum DamageType {
    Physical,
    Magical
}
