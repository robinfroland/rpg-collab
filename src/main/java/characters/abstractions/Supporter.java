package main.java.characters.abstractions;

public interface Supporter {
    double support(Character character);
}