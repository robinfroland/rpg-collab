package main.java.characters.abstractions;

public interface Attacker {
    double attack();
}
