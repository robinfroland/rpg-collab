package main.java.characters.melee;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.characters.MeleeBlade;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponType;

/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue extends MeleeBlade {

    public Rogue() {
        super(
                CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH,
                CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER,
                WeaponFactory.getWeapon(WeaponType.Axe),
                ArmorFactory.getArmor(ArmorType.Leather, 1)
        );
    }
}