package main.java.characters.melee;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.characters.MeleeBlunt;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponType;

/*
 Paladins are faithful servants of the light and everything holy.
 They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 Paladins are very durable and typically wield heavy weapons.
*/
public class Paladin extends MeleeBlunt {

    public Paladin() {
        super(
                CharacterBaseStatsDefensive.PALADIN_BASE_HEALTH,
                CharacterBaseStatsDefensive.PALADIN_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PALADIN_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER,
                WeaponFactory.getWeapon(WeaponType.Hammer),
                ArmorFactory.getArmor(ArmorType.Plate, 1)
        );
    }
}
