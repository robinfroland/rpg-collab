package main.java.characters.melee;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.characters.MeleeBlade;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponType;

/*
 Warriors are combat veterans, durable forces on the battlefield.
 They are masters of the blade and wield it with unmatched ferocity.
*/
public class Warrior extends MeleeBlade {

    public Warrior() {
        super(
                CharacterBaseStatsDefensive.WARRIOR_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARRIOR_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARRIOR_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.WARRIOR_MELEE_ATTACK_POWER,
                WeaponFactory.getWeapon(WeaponType.Dagger),
                ArmorFactory.getArmor(ArmorType.Plate, 1)
        );
    }
}