package main.java.characters.caster;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Attacker;
import main.java.characters.abstractions.characters.Caster;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.DamagingSpell;

/*
 Class description:
 ------------------
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage extends Caster implements Attacker {

    public Mage(DamagingSpell spell) {
        super(
                CharacterBaseStatsDefensive.MAGE_BASE_HEALTH,
                CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.MAGE_MAGIC_POWER,
                WeaponFactory.getWeapon(WeaponType.Staff),
                ArmorFactory.getArmor(ArmorType.Leather, 1),
                spell
        );
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
