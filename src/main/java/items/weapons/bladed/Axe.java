package main.java.items.weapons.bladed;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BladeWeapon;

public class Axe extends BladeWeapon {

    // Constructors
    public Axe() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    // Overload if rarity is provided
    public Axe(double rarity) {
        super(rarity);
    }

    // Public getters
    @Override
    public double getBladedPowerModifier() {
        return WeaponStatsModifiers.AXE_ATTACK_MOD;
    }
}

