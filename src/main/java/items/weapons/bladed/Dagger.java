package main.java.items.weapons.bladed;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BladeWeapon;

public class Dagger extends BladeWeapon {

    // Constructors
    public Dagger() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    // Overload if rarity is provided
    public Dagger(double rarity) {
        super(rarity);
    }

    // Public getters
    @Override
    public double getBladedPowerModifier() {
        return WeaponStatsModifiers.DAGGER_ATTACK_MOD;
    }
}
