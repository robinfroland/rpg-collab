package main.java.items.weapons.bladed;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BladeWeapon;

public class Sword extends BladeWeapon {

    // Constructors
    public Sword() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    // Overload if rarity is provided
    public Sword(double rarity) {
        super(rarity);
    }

    // Public getters

    @Override
    public double getBladedPowerModifier() {
        return WeaponStatsModifiers.SWORD_ATTACK_MOD;
    }
}