package main.java.items.weapons.abstractions;

public abstract class BluntWeapon extends Weapon {
    protected BluntWeapon(double rarity) {
        super(rarity);
    }
    public abstract double getBluntPowerModifier();
}
