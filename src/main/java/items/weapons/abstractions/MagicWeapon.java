package main.java.items.weapons.abstractions;

public abstract class MagicWeapon extends Weapon {
    protected MagicWeapon(double rarity) {
        super(rarity);
    }

    public abstract double getMagicPowerModifier();
}
