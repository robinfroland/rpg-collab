package main.java.items.weapons.abstractions;

import main.java.consolehelpers.Color;

public abstract class Weapon {
    private final double rarity;

    protected Weapon(double rarity) {
        this.rarity = rarity;
    }

    public double getRarity() {
        return rarity;
    }

    @Override
    public String toString() {
        return Color.MAGENTA + getClass().getSimpleName() + Color.RESET;
    }
}

