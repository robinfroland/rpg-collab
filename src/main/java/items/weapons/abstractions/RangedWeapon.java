package main.java.items.weapons.abstractions;

public abstract class RangedWeapon extends Weapon {
    protected RangedWeapon(double rarity) {
        super(rarity);
    }

    public abstract double getRangedPowerModifier();
}
