package main.java.items.weapons.abstractions;

public abstract class BladeWeapon extends Weapon {
    protected BladeWeapon(double rarity) {
        super(rarity);
    }
    public abstract double getBladedPowerModifier();
}
