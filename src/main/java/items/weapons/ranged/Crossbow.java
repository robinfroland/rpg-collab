package main.java.items.weapons.ranged;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Crossbow extends RangedWeapon {
    // Constructors
    public Crossbow() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    // Overload if rarity is provided
    public Crossbow(double rarity) {
        super(rarity);
    }

    @Override
    public double getRangedPowerModifier() {
        return WeaponStatsModifiers.CROSSBOW_ATTACK_MOD;
    }
}