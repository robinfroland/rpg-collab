package main.java.items.weapons.ranged;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Gun extends RangedWeapon {

    // Constructors
    public Gun() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    // Overload if rarity is provided
    public Gun(double rarity) {
        super(rarity);
    }

    @Override
    public double getRangedPowerModifier() {
        return WeaponStatsModifiers.GUN_ATTACK_MOD;
    }
}
