package main.java.items.weapons.ranged;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Bow extends RangedWeapon {

    // Constructors
    public Bow() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    // Overload if rarity is provided
    public Bow(double rarity) {
        super(rarity);
    }

    // Public getters
    @Override
    public double getRangedPowerModifier() {
        return WeaponStatsModifiers.BOW_ATTACK_MOD;
    }
}