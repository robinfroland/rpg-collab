package main.java.items.weapons.magic;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.MagicWeapon;

public class Staff extends MagicWeapon {

    // Constructors
    public Staff() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    // Overload if rarity is provided
    public Staff(double rarity) {
        super(rarity);
    }

    // Public getters
    @Override
    public double getMagicPowerModifier() {
        return WeaponStatsModifiers.STAFF_MAGIC_MOD;
    }
}

