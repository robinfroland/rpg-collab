package main.java.items.weapons.magic;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.MagicWeapon;

public class Wand extends MagicWeapon {

    // Constructors
    public Wand() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    // Overload if rarity is provided
    public Wand(double rarity) {
        super(rarity);
    }

    // Public getters
    @Override
    public double getMagicPowerModifier() {
        return WeaponStatsModifiers.WAND_MAGIC_MOD;
    }
}