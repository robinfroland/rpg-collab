package main.java.items.weapons.blunt;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BluntWeapon;

public class Hammer extends BluntWeapon {

    // Constructors
    public Hammer() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    // Overload if rarity is provided
    public Hammer(double rarity) {
        super(rarity);
    }

    // Public getters
    @Override
    public double getBluntPowerModifier() {
        return WeaponStatsModifiers.HAMMER_ATTACK_MOD;
    }
}