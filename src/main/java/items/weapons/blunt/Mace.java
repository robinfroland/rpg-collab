package main.java.items.weapons.blunt;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BluntWeapon;

public class Mace extends BluntWeapon {
    // Constructors
    public Mace() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    // Overload if rarity is provided
    public Mace(double rarity) {
        super(rarity);
    }

    // Public getters
    @Override
    public double getBluntPowerModifier() {
        return WeaponStatsModifiers.MACE_ATTACK_MOD;
    }
}