package main.java.items.armor.abstractions;

import main.java.consolehelpers.Color;

public abstract class Armor {

    private double healthModifier;
    private double physRedModifier;
    private double magicRedModifier;
    private double rarityModifier;

    public Armor(double rarityModifier, double healthModifier, double physRedModifier, double magicRedModifier) {
        this.healthModifier = healthModifier;
        this.physRedModifier = physRedModifier;
        this.magicRedModifier = magicRedModifier;
        this.rarityModifier = rarityModifier;
    }

    public Armor(double healthModifier, double physRedModifier, double magicRedModifier) {
        this.healthModifier = healthModifier;
        this.physRedModifier = physRedModifier;
        this.magicRedModifier = magicRedModifier;
    }

    public double getHealthModifier() {
        return healthModifier;
    }

    public double getPhysRedModifier() {
        return physRedModifier;
    }

    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    public double getRarityModifier() {
        return rarityModifier;
    }

    @Override
    public String toString() {
        return Color.MAGENTA + getClass().getSimpleName() + Color.RESET;
    }
}
