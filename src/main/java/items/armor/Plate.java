package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;

public class Plate extends Armor {

    // Constructors
    public Plate() {
        super(
                ArmorStatsModifiers.PLATE_HEALTH_MODIFIER,
                ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER);
    }

    public Plate(double itemRarity) {
        super(itemRarity,
                ArmorStatsModifiers.PLATE_HEALTH_MODIFIER,
                ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER);
    }
}
