# RPG Collab
A console RPG application. My role in this collaboration is refactoring the project in order to adhere good principles of object oriented programming.

## Demonstration

![](src/resources/demo01.png)
![](src/resources/demo02.png)